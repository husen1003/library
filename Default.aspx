﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <body style="background-image: url(image/6.jpg);background-size: cover; background-repeat:no-repeat; margin-top:0;">
            <div style="margin:40px 0 0 60px;">
                <div style="background:rgba(1,1,1,0.7);width:35%;border-radius:500px;">
                    <a href="reg.aspx" style="text-decoration:none;font-size:6em"><i class="fa fa-user-plus" aria-hidden="true"></i><font size="50px;">&nbsp Sign Up</font></a>
                </div>
                <br />
                <div style="background:rgba(1,1,1,0.7);width:35%;border-radius:500px;margin-left:400px;padding-left:50px;">
                    <a href="login.aspx" style="text-decoration:none;font-size:6em"><i class="fa fa-arrow-right" aria-hidden="true"></i><font size="50px;" >&nbsp&nbsp&nbsp Sign In</font></a>
                </div>
                <div style="background:rgba(1,1,1,0.7);width:35%;border-radius:500px;float:right;padding-left:50px;">
                    <a href="contact.aspx" style="text-decoration:none;font-size:6em"><i class="fa fa-phone" aria-hidden="true"></i><font size="50px;" >&nbsp&nbsp Contact US</font></a>
                </div>
            </div>
        </body>
</asp:Content>

