﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="verify.aspx.cs" Inherits="verify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">



<script type = "text/javascript">

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
            document.getElementById("error").style.display = ret ? "none" : "inline";
        }
    }



</script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
<body style="background-image: url(image/book.jpg);background-size: cover; background-attachment:fixed;">

<center>
    <h4 style="color:white;">Check <asp:Label ID="email" runat="server" Text="" style="color:powderblue;"></asp:Label><br /> account and get activation code to verify Email</h4>
<div class="form shadow-lg p-4" style="background: rgba(0, 0, 0, 0.8); width:40%; margin-top:30px; border-radius:50px;" >

    <h2 align="center" style="color: white; margin-top:35px;"> Enter Verification Code </h2><br /><br />

				<div class="form-group" align="left">
					<label style="color:white;"><b>Enter Activation Code:-</b></label>
            <asp:TextBox ID="otp" runat="server" required="" MaxLength="6" onkeypress="return isNumberKey(event)" placeholder="Enter Otp" CssClass="form-control" AutoCompleteType="Disabled"></asp:TextBox>
      </div>  
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" style="float:left;" ControlToValidate="otp" ErrorMessage="Enter Valid OTP" ForeColor="#CC0000" ValidationExpression="^\d{6}$"></asp:RegularExpressionValidator>

          <br />


    <asp:Button ID="validate" runat="server" CssClass="btn btn-primary" style="width: 100%" Text="Verify Email" OnClick="validate_Click" />
          <br /><br />
            <span style="float:left;">
              <a href="resend.aspx?email=<%=qrystringemail %>">Resend OTP</a>
          </span>
          <br /><br />
</div>
</center>



</asp:Content>

