﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="show-book.aspx.cs" Inherits="show_book" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




<link href="librarian/assets/DataTables/datatables.css" type="text/css" rel="stylesheet" />
    <script src="librarian/assets/js/jquery-3.3.1.js"></script>
    <script src="librarian/assets/DataTables/datatables.js"></script>

    <div style="height:50px;">

    </div>


    <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
        <HeaderTemplate>
            <table class="table table-hover table-striped" id="example">
                <thead class="thead-dark">
                    <tr>
                    <th>No</th>
                    <th>Book Title</th>
                    <th>publication</th>
                    <th>Stream</th>
                    <th>Author</th>
                    <th>Available Books</th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>


        <ItemTemplate>
            <tr>
                <td><%#Container.ItemIndex+1 %></td>
                <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("title") %>'></asp:Label></td>
                <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("publication") %>'></asp:Label></td>
                <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("stream") %>'></asp:Label></td>
                <td style="text-align:center;"><asp:Label ID="Label4" runat="server" Text='<%# Eval("auther") %>'></asp:Label></td>
                <td style="text-align:center;"><asp:Label ID="Label5" runat="server" Text='<%# Eval("qty") %>'></asp:Label></td>
            </tr>

        </ItemTemplate>


        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    

<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>



</asp:Content>

