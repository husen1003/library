﻿<%@ Page Title="User Registration" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="reg.aspx.cs" Inherits="reg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type = "text/javascript">

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            return false; 
            document.getElementById("error").style.display = ret ? "none" : "inline";      
        } 
    }



</script>

<style>
body{
    background-image: url(image/book.jpg);
    background-size: cover;
    background-attachment:fixed;
}
.marg{
    margin-top:-20px;
}
</style>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<body>
    <center>

<div class="form shadow-lg p-4" style="background: rgba(0, 0, 0, 0.6); width:50%; border-radius:40px; margin-top:30px;" >
			
				<center><h1 style="color:white;"><b>Registration</b></h1>
                    <p style="color:white;">
                    <span style="color:red;"><asp:Label ID="duplicate" runat="server" Text="User Already Exists!" style="float:left; display:none;"></asp:Label></span>
                        <br />
                    <span style="color:red;"><asp:Label ID="Label1" runat="server" Text="" style="float:left;"></asp:Label></span><br />
                    </p></center>
			
		<!--enrno -->
				<div class="form-group" align="left"">
					<label style="color:white;"><b>Enter Enrollment No:-</b></label>
                    <asp:TextBox ID="enrno" runat="server" onkeypress="return isNumberKey(event)" MaxLength="6" required="" CssClass="form-control" placeholder="Enter Last 6 numbers of your Enrollment No" AutoCompleteType="Disabled" ></asp:TextBox>
                    
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="enrno" ErrorMessage="Enter Valid Enrollment Number!! Enter Last 6 numbers of your Enrollment No" ForeColor="#CC0000" ValidationExpression="^\d{6}$"></asp:RegularExpressionValidator>


                </div>
		
				<div class="form-group marg" align="left">
					<label style="color:white;"><b>First Name:-</b></label>
                    <asp:TextBox ID="fname" runat="server" required="" CssClass="form-control" placeholder="Enter First Name" AutoCompleteType="Disabled" ></asp:TextBox>

                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="fname" ErrorMessage="Enter Only Characters" ForeColor="#CC0000" ValidationExpression="[a-zA-Z]+"></asp:RegularExpressionValidator>

                </div>
				<div class="form-group marg" align="left">
					<label style="color:white;"><b>Last Name:-</b></label>
                    <asp:TextBox ID="lname" runat="server" required="" CssClass="form-control" placeholder="Enter Last Name" AutoCompleteType="Disabled" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="lname" ErrorMessage="Enter Only Characters" ForeColor="#CC0000" ValidationExpression="[a-zA-Z]+"></asp:RegularExpressionValidator>
                </div>
    			<div class="form-group marg" align="left">
					<label style="color:white;"><b>Class:-</b></label>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" ValidationGroup="g1">
                        <asp:ListItem Selected="True">--Select--</asp:ListItem>
                        <asp:ListItem>Bcom</asp:ListItem>
                        <asp:ListItem>BBA</asp:ListItem>
                        <asp:ListItem>BCA</asp:ListItem>
                    </asp:DropDownList>                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" InitialValue="--Select--" 
                         ErrorMessage="*Select your stream!" ControlToValidate="DropDownList1" ForeColor="#CC0000"
                         Operator="NotEqual" Type="Integer"></asp:RequiredFieldValidator>
                </div>
				<div class="form-group marg" align="left">
					<label style="color:white;"><b>Mo Number:-</b></label>
                    <asp:TextBox ID="mo" runat="server" required="" CssClass="form-control" onkeypress="return isNumberKey(event)" MaxLength="10" placeholder="Enter Mobile Number" AutoCompleteType="Disabled" ></asp:TextBox>                    
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="mo" ErrorMessage="Enter Valid Mobile Number" ForeColor="#CC0000" ValidationExpression="^\d{10}$"></asp:RegularExpressionValidator>
                </div>
				<div class="form-group marg" align="left">
					<label style="color:white;"><b>Email:</b></label>
                    <asp:TextBox ID="email" runat="server" required="" CssClass="form-control" placeholder="Enter Email" AutoCompleteType="Disabled" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="email" ErrorMessage="Enter Valid Email address" ForeColor="#CC0000" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </div>
			    <div class="form-group marg" align="left">
				    <label style="color:white;"><b>Password:-</b></label>  
                    <asp:TextBox ID="pass" runat="server" required="" CssClass="form-control" placeholder="Enter Password" AutoCompleteType="Disabled" TextMode="Password" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="pass" ErrorMessage="Password size must in 8-15, atleast 1 Uppercase, 1 Lowercase and 1 special character" ForeColor="#CC0000" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$"></asp:RegularExpressionValidator>
                </div> 
			    <div class="form-group marg" align="left">
				    <label style="color:white;"><b>Confirm Password:-</b></label> 
                    <asp:TextBox ID="cpass" runat="server" required="" CssClass="form-control" placeholder="Confirm Password" AutoCompleteType="Disabled" TextMode="Password" ></asp:TextBox>
                </div>        
                
                <br /><br />

                <asp:Button ID="Button1" runat="server" Text="Register" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click1" />  
                <br />      
                
                <br /><br />
                <span style="text-align:center;"><font color="white">If already Register! <a href="login.aspx" style="text-decoration:underline; color:white;">Click here to LOGIN !</a></font></span>

</div>  


    </center>

</body>

</asp:Content>

