﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Net;
using System.Net.Mail;

public partial class verify : System.Web.UI.Page
{
    public string qrystringemail = "";
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!IsPostBack)
        {
            email.Text = Request.QueryString["email"].ToString();
            Session["qrystringemail"] = Request.QueryString["email"].ToString();
        } 
    }
    protected void validate_Click(object sender, EventArgs e)
    {
        qrystringemail = Request.QueryString["email"].ToString();
        string qry = "select * from student where email='" + Session["qrystringemail"].ToString() + "'";
        SqlCommand cmd = new SqlCommand(qry, con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (otp.Text.ToString() == dr["otp"].ToString())
        {
            dr.Close();
            changestatus();
            Response.Write("<script> alert('Email verified Successfully!'); </script>");
            Response.Write("<script> window.location = 'login.aspx'; </script>");            
        }
        else
        {
            Response.Write("<script> alert('Invalid Activation Code!'); </script>");
        }
    }
    public void changestatus()
    {
        string qry = "update student set emailstatus= 'verified' where email= '" + qrystringemail.ToString() + "'";
        SqlCommand cmd = new SqlCommand(qry, con);
        cmd.ExecuteNonQuery();
    }
}