﻿<%@ Page Title="User Login" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <body style="background-image: url(image/background.jpg);background-size: cover;">
<center>
<div class="form shadow-lg p-4" style="background: rgba(0, 0, 0, 0.8); width:40%; margin-top:30px; border-radius:20px;">

    <h2 align="center" style="color: white; margin-top:35px;"> Login Here </h2><br><br>
      <div class="form-group" align="left">

        <label style="color: white;"><b>Mobile, Email or Enrollment number:-</b></label>
            <asp:TextBox ID="TextBox1" runat="server" required="" placeholder="Mobile, Email or Enrollment number:-" CssClass="form-control" AutoCompleteType="Disabled"></asp:TextBox>
          <br />

        <label style="color: white;"><b> Password:- </b></label></h3>        
            <asp:TextBox ID="TextBox2" runat="server" required="" placeholder="password" CssClass="form-control" AutoCompleteType="Disabled" TextMode="Password"></asp:TextBox>
            <span style="color:red;"><asp:Label ID="Label1" runat="server" Text=""></asp:Label></span>
          <br />
          <br />


    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" style="width: 100%" Text="Login" OnClick="Button1_Click" />
          <span>
            <div ID="error" runat="server" class="alert alert-danger" role="alert" style="margin-top:10px; display:none;">
              Invalid Username or Password!
            </div>
          </span>
          <span>
            <div ID="note" runat="server" class="alert alert-danger" role="alert" style="margin-top:10px; display:none;">
              Note:- You have to active your id from Librarian!
            </div>
          </span>
          <br /><br /><br />
          <div  style="text-align:left;">
          <span><font color="white">If not Register!</font> <a href="reg.aspx">Click here to REGISTER !</a></span>
          <span style="float: right;"><a href="forgotpass.aspx">Forgot Password</a></span>
          </div>  
</center>

        </body>

</asp:Content>

