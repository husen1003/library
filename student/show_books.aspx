﻿<%@ Page Title="" Language="C#" MasterPageFile="~/student/MasterPage.master" AutoEventWireup="true" CodeFile="show_books.aspx.cs" Inherits="student_books" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<link href="assets/DataTables/datatables.css" type="text/css" rel="stylesheet" />
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/DataTables/datatables.js"></script>



    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <table class="table table-hover table-striped" id="example">
                <thead class="thead-dark">
                    <tr>
                    <th>Book Title</th>
                    <th>publication</th>
                    <th>Stream</th>
                    <th>Author</th>
                    <th>Available Books</th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>


        <ItemTemplate>
            <tr>
                <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("title") %>'></asp:Label></td>
                <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("publication") %>'></asp:Label></td>
                <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("stream") %>'></asp:Label></td>
                <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("auther") %>'></asp:Label></td>
                <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("qty") %>'></asp:Label></td>
            </tr>

        </ItemTemplate>


        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>

<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>




</asp:Content>

