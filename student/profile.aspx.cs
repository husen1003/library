﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class student_profile : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        if (!Page.IsPostBack)
        {
            string qry = "select * from student where id= '" + Application["stid"].ToString() + "'";
            SqlCommand cmd1 = new SqlCommand(qry, con);
            cmd1.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Session["sid"] = dr["id"].ToString();
                enrno.Text = dr["enrno"].ToString();
                fname.Text = dr["name"].ToString();
                lname.Text = dr["lname"].ToString();
                mo.Text = dr["mo"].ToString();
                email.Text = dr["email"].ToString();
            }
        }
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        string qry = "select * from student where id not in ('" + Session["sid"] + "') and (email = '" + email.Text + "' or mo = '" + mo.Text.ToString() + "')";
        SqlCommand cmd1 = new SqlCommand(qry, con);
        SqlDataReader dr = cmd1.ExecuteReader();
        dr.Read();
        if (dr.HasRows)
        {
            error.Text = "Email ID or Mobile number is already exist!";
        }
        else
        {
            dr.Close();
            string query = "update student set name='" + fname.Text + "', lname='" + lname.Text + "', mo='" + mo.Text + "', email='" + email.Text + "' where id= '" + Application["stid"].ToString() + "'";
            Application["student"] = fname.Text.ToString();
            Application["stmo"] = mo.Text.ToString();
            Application["stemail"] = email.Text.ToString();
            SqlCommand cmd = new SqlCommand(query, con);
            int row = cmd.ExecuteNonQuery();
            if (row > 0)
            {
                Response.Write("<script> alert('Data Updated Successfully!'); </script>");
                Response.Write("<script>window.location = 'default.aspx';</script>");
            }
        }
    }
}