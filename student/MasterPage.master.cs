﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class student_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Application["student"] == null)
        {
            Response.Redirect("../login.aspx");
        }
        String sbstrs = Application["student"].ToString();
        sbstr.Text = sbstrs.Substring(0, 1);
    }
}
