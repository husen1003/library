﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class student_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Application["student"] = null;
        if (Application["student"] == null)
            Response.Redirect("../login.aspx");
    }
}