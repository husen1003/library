﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class student_my_books : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        if (!Page.IsPostBack)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("Student_name");
            dt.Columns.Add("book_name");
            dt.Columns.Add("issue_date");
            dt.Columns.Add("approx_rdate");
            dt.Columns.Add("is_book_return");
            dt.Columns.Add("rdate");
            dt.Columns.Add("latedays");

            string qry1 = "select * from issue_book where student_id='" + Application["stid"].ToString() + "' and return_status = 'no'";
            SqlCommand cmd1 = new SqlCommand(qry1, con);
            cmd1.ExecuteNonQuery();
            DataTable dt1 = new DataTable();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            da1.Fill(dt1);
            foreach (DataRow dr1 in dt1.Rows)
            {
                DataRow dr = dt.NewRow();
                dr["Student_name"] = dr1["student_name"].ToString();
                dr["book_name"] = dr1["book_name"].ToString();
                dr["issue_date"] = dr1["issue_date"].ToString();
                dr["approx_rdate"] = dr1["approx_rdate"].ToString();
                dr["is_book_return"] = dr1["return_status"].ToString();
                dr["rdate"] = dr1["rdate"].ToString();

                DateTime d1 = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd"));
                DateTime d2 = Convert.ToDateTime(dr1["approx_rdate"].ToString());

                if (d1 > d2)
                {
                    TimeSpan t = d1 - d2;
                    double cdays = t.TotalDays;
                    dr["latedays"] = cdays.ToString();
                }
                else
                {
                    dr["latedays"] = 0;
                }
                dt.Rows.Add(dr);
            }
            SqlDataReader sdr = cmd1.ExecuteReader();
            if (sdr.HasRows)
            {
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            else
            {
                nobooks.Style.Add("display", "block");
                //Label1.Text("You have not Issued any books!");
            }
            sdr.Close();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("show_books.aspx");
    }
}