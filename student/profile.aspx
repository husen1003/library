﻿<%@ Page Title="" Language="C#" MasterPageFile="~/student/MasterPage.master" AutoEventWireup="true" CodeFile="profile.aspx.cs" Inherits="student_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script type = "text/javascript">

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
            document.getElementById("error").style.display = ret ? "none" : "inline";
        }
    }



</script>

<style>
    .marg{
        margin-top:-20px;
    }
</style>



    <center>

<div class="form shadow-lg p-4" style="color: black; width:60%;">
			
				<center><h1><b>My Profile</b></h1></center>
			
		
				<div class="form-group" align="left">
					<label><b>Enrollment Number:-</b></label>
                    <asp:TextBox ID="enrno" CssClass="form-control" ReadOnly="true" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div> 		
				<div class="form-group" align="left">
					<label><b>Username:-</b></label>
                    <asp:TextBox ID="fname" CssClass="form-control" placeholder="Enter Name" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>             
				<div class="form-group marg" align="left">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="fname" ErrorMessage="Enter Characters Only" ForeColor="#CC0000" ValidationExpression="[a-zA-Z]+"></asp:RegularExpressionValidator>
                    <br />
					<label><b>Last Name:-</b></label>
                    <asp:TextBox ID="lname" CssClass="form-control" placeholder="Enter Mobile Number" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>   
				<div class="form-group marg" align="left">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="lname" ErrorMessage="Enter Characters Only" ForeColor="#CC0000" ValidationExpression="[a-zA-Z]+"></asp:RegularExpressionValidator> 
                    <br />
					<label><b>Mo Number:-</b></label>
                    <asp:TextBox ID="mo" MaxLength="10" onkeypress="return isNumberKey(event)" CssClass="form-control" placeholder="Enter Mobile Number" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group marg" align="left">
                    
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="mo" ErrorMessage="Enter Valid Mobile Number" ForeColor="#CC0000" ValidationExpression="^\d{10}$"></asp:RegularExpressionValidator>
                    <br />
					<label><b>Email:</b></label>
                    <asp:TextBox ID="email"  CssClass="form-control" placeholder="Enter Email" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="email" ErrorMessage="Enter Valid Email Address" ForeColor="#CC0000" style="margin-top:-40px;" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </div>     
    
                <span style="float:left; color:red; margin-top:-20px;"><asp:Label ID="error" runat="server" Text="" /></span><br />
                <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click1" />         
                
</div>


    

    </center>




</asp:Content>

