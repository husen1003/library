﻿<%@ Page Title="" Language="C#" MasterPageFile="~/student/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="student_my_books" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




    <div class="container-fluid" style="min-height:500px; background-color:white;">

        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table class="table table-hovered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>NO</th>
                            <th>Student Name</th>
                            <th>Book Name</th>
                            <th>Book Issue Date</th>
                            <th>Approx Return Date</th>
                            <th>Late Days</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td><%#Container.ItemIndex+1 %></td>
                    <td><%#Eval("student_name") %></td>
                    <td><%#Eval("book_name") %></td>
                    <td><%#Eval("issue_date") %></td>
                    <td><%#Eval("approx_rdate") %></td>
                    <td><%#Eval("latedays") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>

    <h1 style="text-align:center; display:none;" ID="nobooks" runat="server">You have not any Issued books!</h1>    
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <br />
        <br />
        <center><asp:Button ID="Button1" runat="server" CssClass="btn btn-warning" Text="See All Books of Library" OnClick="Button1_Click" /></center>

    </div>




</asp:Content>

