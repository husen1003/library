﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="update-book.aspx.cs" Inherits="Librarian_update_book" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<style>
    .marg{
        margin-top:-20px;
    }
</style>


                      <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Update Book</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                      <div class="form-group">
                                          <label class="control-label mb-1">Book Title</label>
                                          <asp:TextBox ID="TextBox1" required="" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                          <label class="control-label mb-1">Publication</label>
                                          <asp:DropDownList ID="DropDownList1" class="form-control"  runat="server">
                                              <asp:ListItem Selected="True">--Select--</asp:ListItem>
                                          </asp:DropDownList>
                                      </div>
                                      <div class="form-group">
                                          <label class="control-label mb-1">Stream</label>
                                          <asp:DropDownList ID="DropDownList2" required="" class="form-control" runat="server" AutoPostBack="true" AppendDataBoundItems="true">
                                              <asp:ListItem>BCA</asp:ListItem>
                                              <asp:ListItem>Bcom</asp:ListItem>
                                              <asp:ListItem>BBA</asp:ListItem>
                                          </asp:DropDownList>                    
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="--Select--" 
                                                 ErrorMessage="*Select your stream!" ControlToValidate="DropDownList2" ForeColor="#CC0000"
                                                 Operator="NotEqual" Type="Integer"></asp:RequiredFieldValidator>
                                      </div>
                                      <div class="form-group marg">
                                          <label class="control-label mb-1">Author Name</label>
                                          <asp:TextBox ID="TextBox4" required="" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                          <label class="control-label mb-1">Detail</label>
                                          <asp:TextBox ID="TextBox5" required="" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                          <label class="control-label mb-1">Quantity</label>
                                          <asp:TextBox ID="TextBox6" required="" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div>
                                          <asp:Button ID="Button1" class="btn btn-lg btn-info btn-block" runat="server" Text="Update Book" OnClick="Button1_Click" />
                                          <span>
                                            <div ID="msg" runat="server" class="alert alert-success" role="alert" style="margin-top:10px; display:none;">
                                              Book Updated Successfully!
                                            </div>
                                          </span>
                                      </div>
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div>




    
</asp:Content>

