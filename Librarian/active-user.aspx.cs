﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_active_user : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["activeid"] != null)
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update student set status='yes' where id= '" + Request.QueryString["activeid"].ToString() + "'";
                cmd.ExecuteNonQuery();
                Response.Redirect("show-students.aspx");
            }
            if (Request.QueryString["deavctiveid"] != null)
            {
                String qrycheckissuebook = "select * from issue_book where student_id = '" + Request.QueryString["deavctiveid"].ToString() + "' and return_status = 'no'";
                SqlCommand cmd1 = new SqlCommand(qrycheckissuebook, con);
                SqlDataReader dr = cmd1.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    Response.Write("<script> alert('This user has issued " + dr["book_name"].ToString() + " book, Please Collect this book first before Deactivating this Student! '); </script>");
                    Response.Write("<script> window.location = 'show-students.aspx'; </script>");
                }
                else
                {
                    dr.Close();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "update student set status='no' where id= '" + Request.QueryString["deavctiveid"].ToString() + "'";
                    cmd.ExecuteNonQuery();
                    Response.Redirect("show-students.aspx");
                }
                dr.Close();
            } 
        }
       
    }
}