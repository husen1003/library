﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_profile : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        if (!Page.IsPostBack)
        {
            string qry = "select * from admin where id= '" + Application["adminid"].ToString() + "'";
            SqlCommand cmd1 = new SqlCommand(qry, con);
            cmd1.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                TextBox1.Text = dr["name"].ToString();
                TextBox2.Text = dr["mo"].ToString();
                TextBox3.Text = dr["email"].ToString();
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string query = "update admin set name='" + TextBox1.Text + "', mo='" + TextBox2.Text + "', email='" + TextBox3.Text + "' where id= '" + Application["adminid"].ToString() + "'";
        Application["admin"] = TextBox1.Text.ToString();
        Application["adminmo"] = TextBox2.Text.ToString();
        Application["adminemail"] = TextBox3.Text.ToString();
        SqlCommand cmd = new SqlCommand(query, con);
        int row = cmd.ExecuteNonQuery();
        if (row > 0)
        {
            Response.Write("<script> alert('Data Updated Successfully!'); </script>");
            Response.Write("<script>window.location = 'default.aspx';</script>");
        }

    }
}