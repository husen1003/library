﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="show-students.aspx.cs" Inherits="Librarian_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<link href="assets/DataTables/datatables.css" type="text/css" rel="stylesheet" />
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/DataTables/datatables.js"></script>




    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <table class="table table-hover table-striped" id="example">
                 <thead class="thead-dark">
                     <tr>
                     <th>No</th>                        
                     <th>Name</th>
                     <th>Class</th>
                     <th>Mo:</th>
                     <th style="text-align:center;">E-mail</th>
                     <th>Status</th>
                     <th style="text-align:center;">Active</th>
                     <th style="text-align:center;">Deactive</th>
                     <th style="text-align:center">Edit</th>
                     <th style="text-align:center">Delete</th>
                     </tr>   
                 </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>  
            <a href="">          
                <tr onclick="document.location = 'show-students-detail.aspx?id=<%#Eval("id") %>';" onmouseover="style='cursor: pointer';">
                <td><%#Container.ItemIndex+1 %></td>
                <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("name") %>'></asp:Label></td>
                <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("stream") %>'></asp:Label></td>
                <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("mo") %>'></asp:Label></td>
                <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("email") %>'></asp:Label></td>
                <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("status") %>'></asp:Label></td>
                <td><a href="active-user.aspx?activeid=<%#Eval("id") %>"><center><div class="btn btn-success">Active</div></center></a></td>
                <td><a href="active-user.aspx?deavctiveid=<%#Eval("id") %>"><center><div class="btn btn-danger">Deactive</div></center></a></td>
                <td><a href="edit-user.aspx?id=<%#Eval("id") %>"><center><div class="btn btn-warning">Edit</div></center></a></td>
                <td><a href="delete-admin.aspx?sid=<%#Eval("id") %>"><center><div class="btn btn-danger">Delete</div></center></a></td>
                </tr>
            </a>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>


<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>

</asp:Content>

