﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="sendmail.aspx.cs" Inherits="Librarian_sendmail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <center>

<div class="form shadow-lg p-4" style="color: black; width:60%; background:rgba(0,0,0,0.7);border-radius:30px;margin-top:20px;">
			
				<center style="margin:40px 0px;"><h1><b>Send Message to User</b></h1></center>
			
		
		
				<div class="form-group" align="left">
					<label><b>Email:-</b></label>
                    <asp:TextBox ID="email" CssClass="form-control" placeholder="Enter Name" runat="server" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                </div>                
				<div class="form-group marg" align="left">
					<label><b>Subject:-</b></label>
                    <asp:TextBox ID="sub" CssClass="form-control" required="" placeholder="Enter Subject" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group marg" align="left">
					<label><b>Message:</b></label>
                    <asp:TextBox ID="messsage" required="" TextMode="MultiLine" Rows="7" CssClass="form-control" placeholder="Enter Your Message" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>     
    
                <asp:Button ID="Button1" runat="server" Text="Send Message" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click" />         
                
</div>


    

    </center>

</asp:Content>

