﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="add-publication.aspx.cs" Inherits="Librarian_add_publication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<link href="assets/DataTables/datatables.css" type="text/css" rel="stylesheet" />
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/DataTables/datatables.js"></script>

<center>
    <div class="form-group">
        <asp:TextBox ID="TextBox1" class="form-control" placeholder="Enter Publication Name" runat="server" required=""></asp:TextBox>
    </div>
    <br />
    <asp:Button ID="Button1" runat="server" CssClass="btn btn-info" Text="Add Publication" OnClick="Button1_Click" />
</center>
<br /><br /><br />

    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <table class="table table-hover table-striped" id="example">
                <thead class="thead-dark">
                    <tr>
                    <th>No</th>
                    <th>Publications Name</th>
                    <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("id") %></td>
                <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("name") %>'></asp:Label></td>
                <td><a href="delete-admin.aspx?pid=<%#Eval("id") %>" class="btn btn-danger">Delete</a></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>

<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>


</asp:Content>

