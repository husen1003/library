﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="add-book.aspx.cs" Inherits="Librarian_add_book" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type = "text/javascript">

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
            document.getElementById("error").style.display = ret ? "none" : "inline";
        }
    }



</script>

<style>
    .marg{
        margin-top:-20px;
    }
</style>

                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Add New Book</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                      <div class="form-group">
                                          <label class="control-label mb-1">Book Title</label>
                                          <asp:TextBox ID="title" required="" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                          <label class="control-label mb-1">Publication</label>
                                          <asp:DropDownList ID="DropDownList1" required="" class="form-control" runat="server" AutoPostBack="true" AppendDataBoundItems="true"></asp:DropDownList>                    
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" InitialValue="--Select--" 
                                                 ErrorMessage="*Select your stream!" ControlToValidate="DropDownList1" ForeColor="#CC0000"
                                                 Operator="NotEqual" Type="Integer"></asp:RequiredFieldValidator>
                                      </div>
                                      <div class="form-group marg">
                                          <label class="control-label mb-1">Stream</label>
                                          <asp:DropDownList ID="DropDownList2" required="" class="form-control" runat="server" AutoPostBack="true" AppendDataBoundItems="true">
                                              <asp:ListItem>--Select--</asp:ListItem>
                                              <asp:ListItem>BCA</asp:ListItem>
                                              <asp:ListItem>Bcom</asp:ListItem>
                                              <asp:ListItem>BBA</asp:ListItem>
                                          </asp:DropDownList>                    
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="--Select--" 
                                                 ErrorMessage="*Select your stream!" ControlToValidate="DropDownList2" ForeColor="#CC0000"
                                                 Operator="NotEqual" Type="Integer"></asp:RequiredFieldValidator>
                                      </div>
                                      <div class="form-group marg">
                                          <label class="control-label mb-1">Author Name</label>
                                          <asp:TextBox ID="authname" required="" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                          <label class="control-label mb-1">Detail</label>
                                          <asp:TextBox ID="detail" required="" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                          <label class="control-label mb-1">Quantity</label>
                                          <asp:TextBox ID="qty" required="" MaxLength="4" onkeypress="return isNumberKey(event)" class="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div>
                                          <asp:Button ID="Button1" runat="server" class="btn btn-lg btn-info btn-block" Text="Add New Book" OnClick="Button1_Click" />
                                          <span>
                                            <div ID="msg" runat="server" class="alert alert-success" role="alert" style="margin-top:10px; display:none;">
                                              Book Added Successfully!
                                            </div>
                                          </span>
                                      </div>
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div>




</asp:Content>

