﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_Default : System.Web.UI.Page
{
    public int cstudent = 0;
    public int clibrarian = 0;
    public int cbooks = 0;
    public int creturn = 0;
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!Page.IsPostBack)
        {

            string query = "select * from student where emailstatus='verified'";
            SqlDataAdapter da = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cstudent = dt.Rows.Count;


            string query1 = "select * from admin";
            SqlDataAdapter da1 = new SqlDataAdapter(query1, con);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            clibrarian = dt1.Rows.Count;


            string query2 = "select * from books";
            SqlDataAdapter da2 = new SqlDataAdapter(query2, con);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            cbooks = dt2.Rows.Count;


            string query3 = "select * from issue_book where return_status='no'";
            SqlDataAdapter da3 = new SqlDataAdapter(query3, con);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            creturn = dt3.Rows.Count;

        }
    }
}