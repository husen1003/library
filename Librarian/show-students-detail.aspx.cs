﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_show_students_detail : System.Web.UI.Page
{
    public string uid = "";
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        uid = Request.QueryString["id"].ToString();
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!Page.IsPostBack)
        {
            int flag = 0;
            string query = "select * from student where id = '" + Request.QueryString["id"].ToString() + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            enrno.Text = dr["enrno"].ToString();
            name.Text = dr["name"].ToString();
            lname.Text = dr["lname"].ToString();
            mo.Text = dr["mo"].ToString();
            email.Text = dr["email"].ToString();
            stream.Text = dr["stream"].ToString();
            string query1 = "select * from issue_book where student_name = '" + dr["name"].ToString() + "'";
            string uname = dr["name"].ToString();
            dr.Close();
            SqlDataAdapter da = new SqlDataAdapter(query1, con);
            DataTable dt = new DataTable();
            da.Fill(dt);


            string query2 = "select * from issue_book where student_name = '" + uname.ToString() + "'";
            SqlCommand cmd2 = new SqlCommand(query2, con);
            SqlDataReader dr2 = cmd2.ExecuteReader();
            dr2.Read();

            if (dr2.HasRows)
            {
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            else
            {
                nobooks.Style.Add("display","block");
            }
            dr2.Close();
        }
    }
    protected void sendmail_Click(object sender, EventArgs e)
    {
        Response.Redirect("sendmail.aspx?id="+ Request.QueryString["id"].ToString() +"");
    }
}