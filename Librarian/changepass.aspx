﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="changepass.aspx.cs" Inherits="Librarian_changepass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <center>

<div class="form shadow-lg p-4" style="color: black; width:60%;">
			
				<center><h1><b>Change Password</b></h1></center>
			
		
		
				<div class="form-group" align="left">
					<label><b>Old Password:</b></label>
                    <asp:TextBox ID="TextBox1" CssClass="form-control" placeholder="Old Password" runat="server" AutoCompleteType="Disabled" TextMode="Password" required=""></asp:TextBox>
                </div>     
				<div class="form-group" align="left">
					<label><b>New Password:</b></label>
                    <asp:TextBox ID="TextBox2" CssClass="form-control" placeholder="New Password" runat="server" AutoCompleteType="Disabled" TextMode="Password" required=""></asp:TextBox>
                </div>
				<div class="form-group" align="left">
					<label><b>Confirm New Password:</b></label>
                    <asp:TextBox ID="TextBox3"  CssClass="form-control" placeholder="Confirm New Password" runat="server" AutoCompleteType="Disabled" TextMode="Password" required=""></asp:TextBox>
                    <span ID="error" runat="server" style="display:none; color:red;">Both Password must be same</span>
                    <span ID="error1" runat="server" style="display:none; color:red;">Old Password is Incorrect</span>
                </div>
             
    
                <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click" />         
                
</div>


    

    </center>


</asp:Content>

