﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Net;
using System.Net.Mail;

public partial class Librarian_notice : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue.ToString() == "All")
        {
            sendmail();
        }
        else if(DropDownList1.SelectedValue.ToString() == "Bcom")
        {
            sendmailBcom();
        }
        else if (DropDownList1.SelectedValue.ToString() == "BCA")
        {
            sendmailBca();
        }
        else if (DropDownList1.SelectedValue.ToString() == "BBA")
        {
            sendmailBba();
        }
    }
    public void sendmail()
    {
        string qry = "select * from student where status = 'yes'";
        SqlCommand cmd = new SqlCommand(qry, con);
        DataTable dt1 = new DataTable();
        SqlDataAdapter da1 = new SqlDataAdapter(cmd);
        da1.Fill(dt1);
        foreach (DataRow dr1 in dt1.Rows)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("library.1system@gmail.com", "Abcd@123");
                MailMessage msg = new MailMessage();
                msg.To.Add(dr1["email"].ToString());
                msg.From = new MailAddress("library.1system@gmail.com");
                msg.Subject = "" + sub.Text.ToString() + "";
                msg.Body = "" + messsage.Text.ToString() + "";
                client.Send(msg);
                Response.Write("<script> alert('Message sent Successfully to all students!'); </script>");
                Response.Write("<script> window.location = 'show-students.aspx'; </script>");
            }
            catch (Exception ex)
            {
                Response.Write("<script> alert('Could not send Email ! Please check Your connection!'); </script>");
            }
        }
    }
    public void sendmailBcom()
    {
        string qry = "select * from student where status = 'yes' and stream = '"+ DropDownList1.SelectedValue.ToString() +"'";
        SqlCommand cmd = new SqlCommand(qry, con);
        DataTable dt1 = new DataTable();
        SqlDataAdapter da1 = new SqlDataAdapter(cmd);
        da1.Fill(dt1);
        foreach (DataRow dr1 in dt1.Rows)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("library.1system@gmail.com", "Abcd@123");
                MailMessage msg = new MailMessage();
                msg.To.Add(dr1["email"].ToString());
                msg.From = new MailAddress("library.1system@gmail.com");
                msg.Subject = "" + sub.Text.ToString() + "";
                msg.Body = "Notice:-" + messsage.Text.ToString() + "";
                client.Send(msg);
                Response.Write("<script> alert('Message sent successfully to All students of Bcom!'); </script>");
                Response.Write("<script> window.location = 'show-students.aspx'; </script>");
            }
            catch (Exception ex)
            {
                Response.Write("<script> alert('Could not send Email ! Please check Your connection!'); </script>");
            }
        }
    }
    public void sendmailBca()
    {
        string qry = "select * from student where status = 'yes' and stream = '" + DropDownList1.SelectedValue.ToString() + "'";
        SqlCommand cmd = new SqlCommand(qry, con);
        DataTable dt1 = new DataTable();
        SqlDataAdapter da1 = new SqlDataAdapter(cmd);
        da1.Fill(dt1);
        foreach (DataRow dr1 in dt1.Rows)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("library.1system@gmail.com", "Abcd@123");
                MailMessage msg = new MailMessage();
                msg.To.Add(dr1["email"].ToString());
                msg.From = new MailAddress("library.1system@gmail.com");
                msg.Subject = "" + sub.Text.ToString() + "";
                msg.Body = "" + messsage.Text.ToString() + "";
                client.Send(msg);
                Response.Write("<script> alert('Message sent successfully to All students of BCA!'); </script>");
                Response.Write("<script> window.location = 'show-students.aspx'; </script>");
            }
            catch (Exception ex)
            {
                Response.Write("<script> alert('Could not send Email ! Please check Your connection!'); </script>");
            }
        }
    }
    public void sendmailBba()
    {
        string qry = "select * from student where status = 'yes' and stream = '" + DropDownList1.SelectedValue.ToString() + "'";
        SqlCommand cmd = new SqlCommand(qry, con);
        DataTable dt1 = new DataTable();
        SqlDataAdapter da1 = new SqlDataAdapter(cmd);
        da1.Fill(dt1);
        foreach (DataRow dr1 in dt1.Rows)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("library.1system@gmail.com", "Abcd@123");
                MailMessage msg = new MailMessage();
                msg.To.Add(dr1["email"].ToString());
                msg.From = new MailAddress("library.1system@gmail.com");
                msg.Subject = "" + sub.Text.ToString() + "";
                msg.Body = "" + messsage.Text.ToString() + "";
                client.Send(msg);
                Response.Write("<script> alert('Message sent successfully to All students of BBA!'); </script>");
                Response.Write("<script> window.location = 'show-students.aspx'; </script>");
            }
            catch (Exception ex)
            {
                Response.Write("<script> alert('Could not send Email ! Please check Your connection!'); </script>");
            }
        }
    }
}