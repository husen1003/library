﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Librarian_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




<div class="row" style="text-align: center; font-size: 4em; width: 1373x;">
	<div class="col-4" title="Click Here to see all Students" style="background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;" onclick="window.location.href='show-students.aspx'" onmouseover="style='cursor: pointer; background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;';">
		<%=cstudent %> &nbsp &nbsp <i class="fa fa-users"></i>
        <br>
		Students
	</div>
	<div class="col-4" title="Click Here to see all Admins" style="background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;" onclick="window.location.href='show-admin.aspx'" onmouseover="style='cursor: pointer; background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;';">
		<%=clibrarian %> &nbsp &nbsp <i class="fa fa-users"></i>
        <br>
		Admins
	</div>
	<div class="col-4" title="Click Here to see all Books" style="background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;" onclick="window.location.href='show-books.aspx'" onmouseover="style='cursor: pointer; background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;';">
		<%=cbooks %><br>
		Books
	</div>	
</div>

<div class="row" style="text-align: center; font-size: 4em; width: 1373x; margin-top:5px;">
	<div class="col-6" style="background-color: rgba(0, 0, 0, 0.4);border: 2px solid; border-radius: 25px;" title="Click Here to Issue Books" onclick="window.location.href='issue-book.aspx'" onmouseover="style='cursor: pointer; background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;';">
		<i class="menu-icon fa fa-book"></i>
        <br>
		Issue Books
	</div>
	<div class="col-6" style="background-color: rgba(0, 0, 0, 0.4);border: 2px solid; border-radius: 25px;" title="Click Here to Return books" onclick="window.location.href='return_book_list.aspx'" onmouseover="style='cursor: pointer; background-color: rgba(0, 0, 0, 0.4); border: 2px solid; border-radius: 25px;';">
		<i class="menu-icon fa fa-book"></i>&nbsp &nbsp &nbsp<%=creturn %>
        <br>
		Return Books
	</div>
</div>








</asp:Content>

