﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="edit-user.aspx.cs" Inherits="Librarian_edit_user" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <center>

<div class="form shadow-lg p-4" style="color: black; width:60%;">
			
				<center><h1><b>Update User</b></h1></center>
			
		
				<div class="form-group" align="left">
					<label><b>Enter Enrollment No:-</b></label>
                    <asp:TextBox ID="enrno" required="" MaxLength="6" CssClass="form-control" placeholder="Enter Enrollment No" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>	
				<div class="form-group" align="left">
					<label><b>First name:-</b></label>
                    <asp:TextBox ID="fname" required="" CssClass="form-control" placeholder="Enter First Name" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group" align="left">
					<label><b>Last name:-</b></label>
                    <asp:TextBox ID="lname" required="" CssClass="form-control" placeholder="Enter Last Name" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>                
				<div class="form-group" align="left">
					<label><b>Mo Number:-</b></label>
                    <asp:TextBox ID="mo" required="" MaxLength="10" CssClass="form-control" placeholder="Enter Mobile Number" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group" align="left">
					<label><b>Email:</b></label>
                    <asp:TextBox ID="email" required=""  CssClass="form-control" placeholder="Enter Email" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>     
                <span style="float:left; color:red;"><asp:Label ID="error" runat="server" Text="" /></span><br />
                <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click"  />         
                
</div>


    

    </center>



</asp:Content>

