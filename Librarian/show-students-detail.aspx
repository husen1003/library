﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="show-students-detail.aspx.cs" Inherits="Librarian_show_students_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    



	<table border="1" align="center" class="table table-striped table-hover">
        
	<tr>
		<td align="center"><h1>Student Enrollment Number </h1></td>
		<td><h1><b style="color: green;"> <asp:Label ID="enrno" runat="server" Text=""></asp:Label> </b></h1></td>
	</tr>
	<tr>
		<td align="center"><h1>Student Name </h1></td>
		<td><h1><b style="color: green;"> <asp:Label ID="name" runat="server" Text=""></asp:Label> </b></h1></td>
	</tr>
	<tr>
		<td align="center"><h1>Last Name </h1></td>
		<td><h1><b style="color: green;"> <asp:Label ID="lname" runat="server" Text=""></asp:Label> </b></h1></td>
	</tr>
	<tr>	
		<td align="center"><h2>Mobile Number of Student is </h2></td>
		<td><h2><b style="color: green;"> <asp:Label ID="mo" runat="server" Text=""></asp:Label> </b></h2>
	</tr>
	<tr>	
		<td align="center"><h2>Email address of Student is </h2></td>
		<td><h2><b style="color: green;"> <asp:Label ID="email" runat="server" Text=""></asp:Label> </b></h2></td>
	</tr>	
	<tr>
		<td align="center"><h2>Stream of Student is </h2></td>
		<td><h2><b style="color: green;"> <asp:Label ID="stream" runat="server" Text=""></asp:Label> </b></h2></td>
	</tr>	
	</table>
    <br /><br />
    <center><a href="edit-user.aspx?id=<%=uid %>" class="btn btn-warning">Update Details</a></center>

    <br />
    <br />
    <hr style="" />

    <h1 style="text-align:center">History</h1>

    <br />
    <br />

    <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table class="table table-hovered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>Book Name</th>
                            <th>Book Issue Date</th>
                            <th>Approx Return Date</th>
                            <th>Is Return</th>
                            <th>Return Date</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%#Container.ItemIndex+1 %></td>
                    <td><%#Eval("book_name") %></td>
                    <td><%#Eval("issue_date") %></td>
                    <td><%#Eval("approx_rdate") %></td>
                    <td><%#Eval("return_status") %></td>
                    <td><%#Eval("rdate") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
    </asp:Repeater>
    <br />
    <center><h2 ID="nobooks" runat="server" style="display:none">There is no history Before!</h2></center>
    <br />    <br />
    <center><asp:Button ID="sendmail" runat="server" Text="Send Message" CssClass="btn btn-info" OnClick="sendmail_Click" /></center>
    <br />
</asp:Content>

