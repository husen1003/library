﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_return_book : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    int id;
    string book_name = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        id = Convert.ToInt32(Request.QueryString["id"].ToString());

        string qry = "update issue_book set return_status='yes', rdate='" + DateTime.Now.ToString("yyyy/MM/dd") + "' where student_id=" + id +"";
        SqlCommand cmd = new SqlCommand(qry, con);
        cmd.ExecuteNonQuery();

        string qry1 = "select * from issue_book where student_id=" + id + "";
        SqlCommand cmd1 = new SqlCommand(qry1, con);
        cmd1.ExecuteNonQuery();
        DataTable dt1 = new DataTable();
        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
        da1.Fill(dt1);
        foreach (DataRow dr1 in dt1.Rows)
        {
            book_name = dr1["book_name"].ToString();
        }

        string qry2 = "update books set qty=qty+1 where title='" + book_name.ToString() + "'";
        SqlCommand cmd2 = new SqlCommand(qry2, con);
        cmd2.ExecuteNonQuery();

        Response.Redirect("return_book_list.aspx");
    }
}