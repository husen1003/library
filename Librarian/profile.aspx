﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="profile.aspx.cs" Inherits="Librarian_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script type = "text/javascript">

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
            document.getElementById("error").style.display = ret ? "none" : "inline";
        }
    }



</script>

<style>
    .marg{
        margin-top:-20px;
    }
</style>

    <center>

<div class="form shadow-lg p-4" style="color: black; width:60%;">
			
				<center><h1><b>Librarian Profile</b></h1></center>
			
		
		
				<div class="form-group" align="left">
					<label><b>Username:-</b></label>
                    <asp:TextBox ID="TextBox1" CssClass="form-control" placeholder="Enter Name" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>                
				<div class="form-group marg" align="left">
					<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBox1" ErrorMessage="Enter Characters Only" ForeColor="#CC0000" ValidationExpression="[a-zA-Z]+"></asp:RegularExpressionValidator>
                    <br />
					<label><b>Mo Number:-</b></label>
                    <asp:TextBox ID="TextBox2" MaxLength="10" onkeypress="return isNumberKey(event)" CssClass="form-control" placeholder="Enter Mobile Number" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group marg" align="left">
					<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox2" ErrorMessage="Enter Valid Mobile Number" ForeColor="#CC0000" ValidationExpression="^\d{10}$"></asp:RegularExpressionValidator>
                    <br />
					<label><b>Email:</b></label>
                    <asp:TextBox ID="TextBox3"  CssClass="form-control" placeholder="Enter Email" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox3" ErrorMessage="Enter Valid Email Address" ForeColor="#CC0000" style="margin-top:-40px;" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </div>     
    
                <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click"  />         
                
</div>


    

    </center>

</asp:Content>

