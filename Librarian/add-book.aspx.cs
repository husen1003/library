﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_add_book : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!Page.IsPostBack)
        {
            BindDataSetData();
        }        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        insert();
        Response.Redirect("show-books.aspx");
    }
    public void insert()
    {
        string query = "insert into books values ('" + title.Text + "', '" + DropDownList1.SelectedItem + "', '" + DropDownList2.SelectedItem + "', '" + authname.Text + "', '" + detail.Text + "', '" + qty.Text + "')";
        SqlCommand cmd = new SqlCommand(query, con);
        int row = cmd.ExecuteNonQuery();
        if (row > 0)
        {
            Response.Write("<script>alert('Data inserted successfully');</script>");
        }
        else
            Response.Write("<script>alert('Data not inserted');</script>");
    }
    private void BindDataSetData()
    {
        SqlDataAdapter da = new SqlDataAdapter("select * from publication", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        DropDownList1.DataValueField = "id";
        DropDownList1.DataTextField = "name";
        DropDownList1.Items.Insert(0, "--Select--");
        DropDownList1.DataSource = ds;
        DropDownList1.DataBind();
    }
}