﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="Librarian_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table class="table table-hover table-striped">
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1">
        <Columns>
        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
        <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
        <asp:BoundField DataField="publication" HeaderText="publication" SortExpression="publication" />
        <asp:BoundField DataField="stream" HeaderText="stream" SortExpression="stream" />
        <asp:BoundField DataField="auther" HeaderText="auther" SortExpression="auther" />
        <asp:BoundField DataField="details" HeaderText="details" SortExpression="details" />
        <asp:BoundField DataField="qty" HeaderText="qty" SortExpression="qty" />
        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
    </Columns>
    </asp:GridView>
</table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [books] WHERE [Id] = @Id" InsertCommand="INSERT INTO [books] ([title], [publication], [stream], [auther], [details], [qty]) VALUES (@title, @publication, @stream, @auther, @details, @qty)" SelectCommand="SELECT * FROM [books]" UpdateCommand="UPDATE [books] SET [title] = @title, [publication] = @publication, [stream] = @stream, [auther] = @auther, [details] = @details, [qty] = @qty WHERE [Id] = @Id">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="title" Type="String" />
            <asp:Parameter Name="publication" Type="String" />
            <asp:Parameter Name="stream" Type="String" />
            <asp:Parameter Name="auther" Type="String" />
            <asp:Parameter Name="details" Type="String" />
            <asp:Parameter Name="qty" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="title" Type="String" />
            <asp:Parameter Name="publication" Type="String" />
            <asp:Parameter Name="stream" Type="String" />
            <asp:Parameter Name="auther" Type="String" />
            <asp:Parameter Name="details" Type="String" />
            <asp:Parameter Name="qty" Type="String" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

