﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="addadmin.aspx.cs" Inherits="Librarian_addadmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<head>
    <style>
    .marg{
        margin-top:-20px;
    }
    .white{
        color:white;
    }
    </style>
</head>
<script type = "text/javascript">

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
            document.getElementById("error").style.display = ret ? "none" : "inline";
        }
    }



</script>


    <center>

<div class="form shadow-lg p-4" style="background: rgba(0, 0, 0, 0.8);border-radius:50px; color: black; width:60%;">
			
				<center><h1><b class="white">Add new Admin</b></h1></center>
			
		
		
				<div class="form-group white" align="left">
					<label><b>Username:-</b></label>
                    <asp:TextBox ID="name" CssClass="form-control" required="" placeholder="Enter Name" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group marg white" align="left">
					<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter Only Characters" ControlToValidate="name" ForeColor="#CC0000" ValidationExpression="[a-zA-Z]+"></asp:RegularExpressionValidator>
                    <br />
					<label><b>Mo Number:-</b></label>
                    <asp:TextBox ID="mo" CssClass="form-control" required="" MaxLength="10" onkeypress="return isNumberKey(event)" placeholder="Enter Mobile Number" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group marg white" align="left">
					<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="mo" ErrorMessage="Enter Valid Mobile Number" ForeColor="#CC0000" ValidationExpression="^\d{10}$"></asp:RegularExpressionValidator>
                    <br />
					<label><b>Email:</b></label>
                    <asp:TextBox ID="email"  CssClass="form-control" required="" placeholder="Enter Email" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
			    <div class="form-group marg white" align="left">
				    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="email" ErrorMessage="Enter Valid Email Address" ForeColor="#CC0000" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <br />
				    <label><b>Password:-</b></label>  
                    <asp:TextBox ID="pass"  CssClass="form-control" required="" placeholder="Enter Password" runat="server" AutoCompleteType="Disabled" TextMode="Password"></asp:TextBox>
                </div> 
			    <div class="form-group marg white" align="left">
				    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="pass" ErrorMessage="Password size must in 8-15, atleast 1 Uppercase, 1 Lowercase and 1 special character" ForeColor="#CC0000" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$"></asp:RegularExpressionValidator>
                    <br />
				    <label><b>Confirm Password:-</b></label>
                    <asp:TextBox ID="cpass" CssClass="form-control" required="" placeholder="Confirm Password" runat="server" AutoCompleteType="Disabled" TextMode="Password"></asp:TextBox>
                </div>        
            <asp:Button ID="Button1" runat="server" Text="Add Admin" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click"></asp:Button>
                
</div>


    

    </center>



</asp:Content>

