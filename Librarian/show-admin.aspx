﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="show-admin.aspx.cs" Inherits="Librarian_show_admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
<link href="assets/DataTables/datatables.css" type="text/css" rel="stylesheet" />
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/DataTables/datatables.js"></script>



    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <br />
            <br />
            <br />
            <table class="table table-hover table-striped" id="example">
                 <thead class="thead-dark">
                     <tr>
                         <th>No</th>
                         <th>Name</th>
                         <th>Mobile NO</th>
                         <th>E-mail</th>
                         <th>Remove</th>
                     </tr>
                 </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%#Container.ItemIndex+1 %></td>
                <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("name") %>'></asp:Label></td>
                <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("mo") %>'></asp:Label></td>
                <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("email") %>'></asp:Label></td>
                <td style="text-align:center"><a href="delete-admin.aspx?id=<%#Eval("id") %>" class="btn btn-danger">Delete</a></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>

<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>


</asp:Content>

