﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Librarian_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Application["admin"] = null;
        if (Application["admin"] == null)
            Response.Redirect("../login.aspx");
    }
}