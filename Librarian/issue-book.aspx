﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="issue-book.aspx.cs" Inherits="Librarian_issue_book" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <center>

<div class="form shadow-lg p-4" style="background: rgba(0, 0, 0, 0.8); width:60%;">
			
				<center><h1 style="color:white;"><b>Issue Book</b></h1></center>
			
		
		
				<div class="form-group" align="left">
					<label style="color:white;"><b>Select Student</b></label>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
                </div>
    
				<div class="form-group" align="left">
					<label style="color:white;"><b>Enrollment Number</b></label>
                    <asp:TextBox ID="enrno"  CssClass="form-control" runat="server" AutoCompleteType="Disabled" ReadOnly="True"></asp:TextBox>
                </div>
				<div class="form-group" align="left">
					<label style="color:white;"><b>Last Name</b></label>
                    <asp:TextBox ID="TextBox5"  CssClass="form-control" runat="server" AutoCompleteType="Disabled" ReadOnly="True"></asp:TextBox>
                </div>
				<div class="form-group" align="left">
					<label style="color:white;"><b>Mobile no</b></label>
                    <asp:TextBox ID="TextBox3"  CssClass="form-control" runat="server" AutoCompleteType="Disabled" ReadOnly="True"></asp:TextBox>
                </div>
				<div class="form-group" align="left">
					<label style="color:white;"><b>Email</b></label>
                    <asp:TextBox ID="TextBox4"  CssClass="form-control" runat="server" AutoCompleteType="Disabled" ReadOnly="True"></asp:TextBox>
                </div>
				<div class="form-group" align="left">
					<label style="color:white;"><b>Select Book</b></label>
                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"></asp:DropDownList>
                    </div>
				<div class="form-group" align="left">
					<label style="color:white;"><b>Publication</b></label>
                    <asp:TextBox ID="TextBox1"  CssClass="form-control" runat="server" AutoCompleteType="Disabled" ReadOnly="True"></asp:TextBox>
                </div>    
				<div class="form-group" align="left">
					<label style="color:white;"><b>Available Books</b></label>
                    <asp:TextBox ID="TextBox2"  CssClass="form-control" runat="server" AutoCompleteType="Disabled" ReadOnly="True"></asp:TextBox>
                </div>      
    
                <asp:Button ID="Button1" runat="server" Text="Issue Book" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click" />         
                
</div>  
</center>


</asp:Content>

