﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_changepass : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        if (!Page.IsPostBack)
        {

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string qry = "select * from admin where id= '" + Application["adminid"].ToString() + "'";
        SqlCommand cmd = new SqlCommand(qry, con);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        if (TextBox1.Text.ToString() == dr["pass"].ToString())
        {
            dr.Close();
            if (TextBox2.Text == TextBox3.Text)
            {
                string qry1 = "update admin set pass = '" + TextBox2.Text.ToString() + "' where id= '" + Application["adminid"].ToString() + "'";
                SqlCommand cmd1 = new SqlCommand(qry1, con);
                cmd1.ExecuteNonQuery();
                Response.Write("<script> alert('Password Updated Successfully!'); </script>");
                Response.Write("<script>window.location = 'default.aspx';</script>");
            }
            else
            {
                Response.Write("<script> alert('Both Password Must be same!!'); </script>");
            }

        }
        else
        {
            Response.Write("<script> alert('Incorrect Password!'); </script>");
        } 
    }
}