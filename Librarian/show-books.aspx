﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="show-books.aspx.cs" Inherits="Librarian_show_books" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<link href="assets/DataTables/datatables.css" type="text/css" rel="stylesheet" />
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/DataTables/datatables.js"></script>



    <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
        <HeaderTemplate>
            <table class="table table-hover table-striped" id="example">
                <thead class="thead-dark">
                    <tr>
                    <th>No</th>
                    <th>Book Title</th>
                    <th>publication</th>
                    <th>Stream</th>
                    <th>Author</th>
                    <th>Available Books</th>
                    <th>Update</th>
                    <th>Remove Book</th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>


        <ItemTemplate>
            <tr>
                <td><%#Container.ItemIndex+1 %></td>
                <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("title") %>'></asp:Label></td>
                <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("publication") %>'></asp:Label></td>
                <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("stream") %>'></asp:Label></td>
                <td style="text-align:center;"><asp:Label ID="Label4" runat="server" Text='<%# Eval("auther") %>'></asp:Label></td>
                <td style="text-align:center;"><asp:Label ID="Label5" runat="server" Text='<%# Eval("qty") %>'></asp:Label></td>
                <td><a href="update-book.aspx?id=<%# Eval("id") %>" class="btn btn-info">Update</a></td>
                <td><a href="delete-admin.aspx?bid=<%# Eval("id") %>" class="btn btn-danger">Remove</a></td>
            </tr>

        </ItemTemplate>


        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <center><asp:Button ID="addbook" class="btn btn-success" runat="server" Text="Add New Book" OnClick="addbook_Click" /></center>


<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>



</asp:Content>

