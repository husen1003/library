﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="notice.aspx.cs" Inherits="Librarian_notice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <center>

<div class="form shadow-lg p-4" style="color: black; width:60%; background:rgba(0,0,0,0.7);border-radius:30px;margin-top:20px;">
			
				<center style="margin:40px 0px;"><h1><b>Send Notice to All User</b></h1></center>
			
		
    			<div class="form-group marg" align="left">
					<label style="color:white;"><b>Class:-</b></label>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" ValidationGroup="g1">
                        <asp:ListItem Selected="True">--Select--</asp:ListItem>
                        <asp:ListItem Value="All">All Students</asp:ListItem>
                        <asp:ListItem Value="Bcom">Bcom</asp:ListItem>
                        <asp:ListItem Value="BBA">BBA</asp:ListItem>
                        <asp:ListItem Value="BCA">BCA</asp:ListItem>
                    </asp:DropDownList>                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" InitialValue="--Select--" 
                         ErrorMessage="*Select your One!" ControlToValidate="DropDownList1" ForeColor="#CC0000"
                         Operator="NotEqual" Type="Integer"></asp:RequiredFieldValidator>
                </div>		             
				<div class="form-group" style="margin-top:-20px;" align="left">
					<label><b>Subject:-</b></label>
                    <asp:TextBox ID="sub" CssClass="form-control" required="" placeholder="Enter Subject" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
				<div class="form-group marg" align="left">
					<label><b>Message:</b></label>
                    <asp:TextBox ID="messsage" required="" TextMode="MultiLine" Rows="7" CssClass="form-control" placeholder="Enter Your Message" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                </div>     
    
                <asp:Button ID="Button1" runat="server" Text="Send Notice" CssClass="btn btn-success" style="width: 50%;" OnClick="Button1_Click" />         
                
</div>


    

    </center>


</asp:Content>

