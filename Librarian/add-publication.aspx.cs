﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_add_publication : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!Page.IsPostBack)
        {
            TextBox1.Text = "";
            viewdata();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string query = "insert into publication values ('" + TextBox1.Text + "')";
        SqlCommand cmd = new SqlCommand(query, con);
        int row = cmd.ExecuteNonQuery();
        TextBox1.Text = "";
        viewdata();
    }
    public void viewdata()
    {
        string query = "select * from publication";
        SqlDataAdapter da = new SqlDataAdapter(query, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
}