﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_delete_admin : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        if (Request.QueryString["id"] != null)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete admin where id= '" + Request.QueryString["id"].ToString() + "'";
            cmd.ExecuteNonQuery();
            Response.Redirect("show-admin.aspx");
        }
        if (Request.QueryString["sid"] != null)
        {
            String qrycheckissuebook = "select * from issue_book where student_id = '"+ Request.QueryString["sid"].ToString() +"' and return_status = 'no'";
            SqlCommand cmd1 = new SqlCommand(qrycheckissuebook, con);
            SqlDataReader dr = cmd1.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                Response.Write("<script> alert('This user has issued " + dr["book_name"].ToString() + " book, Please Collect this book first before remove this Student! '); </script>");
                Response.Write("<script> window.location = 'show-students.aspx'; </script>");
            }
            else
            {
                dr.Close();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "delete student where id= '" + Request.QueryString["sid"].ToString() + "'";
                cmd.ExecuteNonQuery();
                Response.Redirect("default.aspx");
            }
            dr.Close();
        }
        if (Request.QueryString["bid"] != null)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete books where id= '" + Request.QueryString["bid"].ToString() + "'";
            cmd.ExecuteNonQuery();
            Response.Redirect("show-books.aspx");
        }
        if (Request.QueryString["pid"] != null)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete publication where id= '" + Request.QueryString["pid"].ToString() + "'";
            cmd.ExecuteNonQuery();
            Response.Redirect("add-publication.aspx");
        }
    }
}