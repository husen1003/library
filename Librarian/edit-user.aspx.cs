﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_edit_user : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!Page.IsPostBack)
        {
            string query = "select * from student where id= '" + Request.QueryString["id"].ToString() + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                enrno.Text = dr["enrno"].ToString();
                Session["sid"] = dr["id"].ToString();
                fname.Text = dr["name"].ToString();
                lname.Text = dr["lname"].ToString();
                mo.Text = dr["mo"].ToString();
                email.Text = dr["email"].ToString();
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String checkenrlno = "select * from student where id not in ('" + Session["sid"] + "') and (enrno = '" + enrno.Text + "' and emailstatus='verified')";
        SqlCommand enrlnocmd = new SqlCommand(checkenrlno, con);
        SqlDataReader enrlnodr = enrlnocmd.ExecuteReader();
        if (enrlnodr.HasRows)
        {
            Response.Write("<script> alert('Enrollment Number is Already Exixts!!'); </script>");
        }
        else
        {
            enrlnodr.Close();
            string qry = "select * from student where id not in ('" + Session["sid"] + "') and (email = '" + email.Text + "' or mo = '" + mo.Text.ToString() + "')";
            SqlCommand cmd1 = new SqlCommand(qry, con);
            SqlDataReader dr = cmd1.ExecuteReader();
            dr.Read();
            if (dr.HasRows)
            {
                error.Text = "Email ID or Mobile number is already exist!";
            }
            else
            {
                dr.Close();
                update();
                Response.Redirect("show-students.aspx");
            }
        }
    }
    public void update()
    {
        string query = "update student Set name='" + fname.Text + "', lname= '"+ lname.Text +"',enrno = '"+ enrno.Text.ToString() +"', mo='" + mo.Text + "', email='" + email.Text + "' where id= '" + Request.QueryString["id"].ToString() + "'  ";
        SqlCommand cmd = new SqlCommand(query, con);
        int row = cmd.ExecuteNonQuery();
        if (row > 0)
        {

        }
        else
        {
            Response.Write("<script>alert('Data not inserted');</script>");
        }
    }
}