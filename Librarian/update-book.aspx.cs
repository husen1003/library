﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_update_book : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!Page.IsPostBack)
        {
            BindDataSetData();
            string qry = "select * from books where id= '" + Request.QueryString["id"].ToString() + "'";
            SqlCommand cmd1 = new SqlCommand(qry, con);
            cmd1.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                TextBox1.Text = dr["title"].ToString();
                DropDownList1.SelectedValue = dr["publication"].ToString();
                DropDownList2.SelectedValue = dr["stream"].ToString();
                TextBox4.Text = dr["auther"].ToString();
                TextBox5.Text = dr["details"].ToString();
                TextBox6.Text = dr["qty"].ToString();
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string query = "update books set title='" + TextBox1.Text + "', publication='" + DropDownList1.SelectedItem + "', stream='" + DropDownList2.SelectedItem + "', auther='" + TextBox4.Text + "', details='" + TextBox5.Text + "', qty='" + TextBox6.Text + "' where id= '" + Request.QueryString["id"].ToString() + "'  ";
        SqlCommand cmd = new SqlCommand(query, con);
        int row = cmd.ExecuteNonQuery();
        Response.Redirect("show-books.aspx");
    }
    private void BindDataSetData()
    {
        SqlDataAdapter da = new SqlDataAdapter("select * from publication", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        DropDownList1.DataValueField = "id";
        DropDownList1.DataTextField = "name";
        DropDownList1.DataSource = ds;
        DropDownList1.DataBind();
    }
}