﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_return_book : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();

        if (!Page.IsPostBack)
        {
            string qry = "select * from issue_book where return_status='no'";
            SqlCommand cmd = new SqlCommand(qry, con);
            SqlDataReader sdr = cmd.ExecuteReader();
            sdr.Read();
            if (sdr.HasRows)
            {
                sdr.Close();
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("id");
                dt.Columns.Add("student_id");
                dt.Columns.Add("Student_name");
                dt.Columns.Add("book_name");
                dt.Columns.Add("issue_date");
                dt.Columns.Add("approx_rdate");
                dt.Columns.Add("is_book_return");
                dt.Columns.Add("rdate");
                dt.Columns.Add("latedays");

                string qry1 = "select * from issue_book where return_status='no'";
                SqlCommand cmd1 = new SqlCommand(qry1, con);
                cmd1.ExecuteNonQuery();
                DataTable dt1 = new DataTable();
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                da1.Fill(dt1);
                foreach (DataRow dr1 in dt1.Rows)
                {
                    DataRow dr = dt.NewRow();
                    dr["id"] = dr1["id"].ToString();
                    dr["student_id"] = dr1["student_id"].ToString();
                    dr["Student_name"] = dr1["student_name"].ToString();
                    dr["book_name"] = dr1["book_name"].ToString();
                    dr["issue_date"] = dr1["issue_date"].ToString();
                    dr["approx_rdate"] = dr1["approx_rdate"].ToString();
                    dr["is_book_return"] = dr1["return_status"].ToString();
                    dr["rdate"] = dr1["rdate"].ToString();

                    DateTime d1 = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd"));
                    DateTime d2 = Convert.ToDateTime(dr1["approx_rdate"].ToString());

                    if (d1 > d2)
                    {
                        TimeSpan t = d1 - d2;
                        double cdays = t.TotalDays;
                        dr["latedays"] = cdays.ToString();
                    }
                    else
                    {
                        dr["latedays"] = 0;
                    }
                    dt.Rows.Add(dr);
                }

                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            else
            {
                nobooks.Style.Add("display", "block");
            }
        }
    }
}