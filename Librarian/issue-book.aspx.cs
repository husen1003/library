﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Librarian_issue_book : System.Web.UI.Page
{
    String selecteduid;
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();
        if (!Page.IsPostBack)
        {
            DropDownList1.Items.Clear();
            DropDownList1.Items.Add("Select");
            string qry = "select * from student";
            SqlCommand cmd = new SqlCommand(qry, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                DropDownList1.Items.Add(dr["name"].ToString());
            }


            DropDownList2.Items.Clear();
            DropDownList2.Items.Add("Select");
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedItem.ToString() == "Select")
        {
            Response.Write("<script>    alert('Please Select Student!!');     </script>");
        }
        else if (DropDownList2.SelectedItem.ToString() == "Select")
        {
            Response.Write("<script>    alert('Please Select Book!!');     </script>");
        }
        else
        {        
            int found = 0;
            string qry = "select * from issue_book where student_name='" + DropDownList1.SelectedItem.ToString() + "' and book_name='" + DropDownList2.SelectedItem.ToString() + "' and return_status='no'";
            SqlCommand cmd = new SqlCommand(qry, con);
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            found = Convert.ToInt32(dt.Rows.Count.ToString());
            if (found > 0)
            {
                Response.Write("<script>    alert('This Book is already exist with this student');     </script>");
            }
            else
            {
                  

                if (TextBox2.Text == "0")
                {
                    Response.Write("<script>    alert('This Book is not available!');     </script>");
                }
                else
                {        
                    string issue_date = DateTime.Now.ToString("yyyy/MM/dd");
                    string approx_rdate = DateTime.Now.AddDays(10).ToString("yyyy/MM/dd");
                    string username = "";
                    string qry2 = "select * from student where name='" + DropDownList1.SelectedItem.ToString() + "'";
                    SqlCommand cmd2 = new SqlCommand(qry2, con);
                    cmd2.ExecuteNonQuery();
                    DataTable dt2 = new DataTable();
                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                    da2.Fill(dt2);
                    foreach (DataRow dr2 in dt2.Rows)
                    {
                        username = dr2["name"].ToString();
                    }

                    string qry3 = "insert into issue_book values('" + Session["selecteduid"].ToString() + "', '" + username.ToString() + "', '" + DropDownList2.SelectedItem.ToString() + "', '" + issue_date.ToString() + "', '" + approx_rdate.ToString() + "','no','no')";
                    SqlCommand cmd3 = new SqlCommand(qry3, con);
                    cmd3.ExecuteNonQuery();

                    string qry4 = "update books set qty=qty-1 where title='" + DropDownList2.SelectedItem.ToString() + "'";
                    SqlCommand cmd4 = new SqlCommand(qry4, con);
                    cmd4.ExecuteNonQuery();

                    Response.Write("<script>    alert('Book Issued Successfully!'); window.location.href=window.location.href;    </script>");
                }
            }
        }
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "select * from student where name = '" + DropDownList1.SelectedItem.ToString() + "' and status = 'no'";
        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataReader sdr = cmd.ExecuteReader();
        sdr.Read();
        if (sdr.HasRows)
        {
            Response.Write("<script> alert('This Student is Deactive! Please Active this user and after issue book.'); </script>");
            Response.Write("<script> window.location = 'show-students.aspx'; </script>");
        }
        else
        {
            sdr.Close();
            TextBox1.Text = "";
            TextBox2.Text = "";
            DropDownList2.Items.Clear();
            DropDownList2.Items.Add("Select");
            string qry1 = "select * from student where name = '" + DropDownList1.SelectedItem.ToString() + "'";
            SqlCommand cmd1 = new SqlCommand(qry1, con);
            SqlDataReader dr = cmd1.ExecuteReader();
            dr.Read();
            string qry2 = "select * from books where stream = '" + dr["stream"].ToString() + "'";
            dr.Close();
            SqlCommand cmd2 = new SqlCommand(qry2, con);
            cmd2.ExecuteNonQuery();
            DataTable dt2 = new DataTable();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            da2.Fill(dt2);
            foreach (DataRow dr2 in dt2.Rows)
            {
                DropDownList2.Items.Add(dr2["title"].ToString());
            }



            cmd1.ExecuteNonQuery();
            DataTable dt1 = new DataTable();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            da1.Fill(dt1);
            foreach (DataRow dr1 in dt1.Rows)
            {
                Session["selecteduid"] = dr1["id"].ToString();
                enrno.Text = dr1["enrno"].ToString();
                TextBox3.Text = dr1["mo"].ToString();
                TextBox4.Text = dr1["email"].ToString();
                TextBox5.Text = dr1["lname"].ToString();
            }

            String already_issued = "select * from issue_book where student_id = '"+ Session["selecteduid"].ToString() +"' and return_status = 'no'";
            SqlCommand cmd_already_issued = new SqlCommand(already_issued, con);
            SqlDataReader dr_already_issued = cmd_already_issued.ExecuteReader();
            dr_already_issued.Read();
            if (dr_already_issued.HasRows)
            {
                Response.Write("<script> alert('This Student has already issued 1 book!'); </script>");
                Response.Write("<script> window.location = 'return_book_list.aspx'; </script>");
            }
            dr.Close();
        }
    }
    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        TextBox1.Text = "";
        TextBox2.Text = ""; 
        string qry2 = "select * from books where title='" + DropDownList2.SelectedItem.ToString() + "'";
        SqlCommand cmd2 = new SqlCommand(qry2, con);
        cmd2.ExecuteNonQuery();
        DataTable dt2 = new DataTable();
        SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
        da2.Fill(dt2);
        foreach (DataRow dr2 in dt2.Rows)
        {
            TextBox1.Text = dr2["publication"].ToString();
            TextBox2.Text = dr2["qty"].ToString();
        }

    }
}