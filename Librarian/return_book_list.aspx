﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Librarian/MasterPage.master" AutoEventWireup="true" CodeFile="return_book_list.aspx.cs" Inherits="Librarian_return_book" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<link href="assets/DataTables/datatables.css" type="text/css" rel="stylesheet" />
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script src="assets/DataTables/datatables.js"></script>

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Return Books</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="min-height:500px; background-color:white;">

        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table class="table table-hovered table-striped" id="example">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>Student Name</th>
                            <th>Book Name</th>
                            <th>Book Issue Date</th>
                            <th>Approx Return Date</th>
                            <th>Late Days</th>
                            <th>Return Book</th>
                            <th>User Details</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td><%#Container.ItemIndex+1 %></td>
                    <td><%#Eval("student_name") %></td>
                    <td><%#Eval("book_name") %></td>
                    <td><%#Eval("issue_date") %></td>
                    <td><%#Eval("approx_rdate") %></td>
                    <td><%#Eval("latedays") %></td>
                    <td><a href="return_book.aspx?id=<%#Eval("student_id") %>" class="btn btn-success">Return Book</a></td>
                    <td><a href="show-students-detail.aspx?id=<%#Eval("student_id") %>" class="btn btn-info">User Info</a></td>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>


        <h1 style="text-align:center; display:none;" ID="nobooks" runat="server">There are not any books to return!</h1>


    </div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>


</asp:Content>

