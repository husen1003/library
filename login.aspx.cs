﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Net;
using System.Net.Mail;

public partial class login : System.Web.UI.Page
{
    static string votp;
    SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }
        con.Open();


        if (Application["admin"] != null)
        {
            Response.Redirect("Librarian");
        }
        if (Application["student"] != null)
        {
            Response.Redirect("student");
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Random random = new Random();
        votp = random.Next(100001, 999999).ToString();
        string qryunverified = "select * from student where email = '" + TextBox1.Text.ToString() + "' and pass = '" + TextBox2.Text.ToString() + "' and emailstatus = 'unverified'";
        SqlCommand cmdunverified = new SqlCommand(qryunverified, con);
        SqlDataReader drverified = cmdunverified.ExecuteReader();
        drverified.Read();
        if (drverified.HasRows)
        {
            drverified.Close();
            string qryupdate = "update student set otp='" + votp.ToString() + "' where email='" + TextBox1.Text + "'";
            SqlCommand cmdupdate = new SqlCommand(qryupdate, con);
            cmdupdate.ExecuteNonQuery();

            Response.Write("<script> alert('You have not verified your email yet! Please check your email we have sent an Activation Code!'); </script>");
            sendmail();
            Response.Redirect("verify.aspx?email=" + TextBox1.Text.ToString() +"");
        }
        drverified.Close();

        string query3 = "select * from student where (email = '" + TextBox1.Text.ToString() + "' or enrno = '" + TextBox1.Text.ToString() + "' or mo = '" + TextBox1.Text.ToString() + "') and status = 'no'";
        SqlCommand cmd3 = new SqlCommand(query3, con);
        SqlDataReader dr3 = cmd3.ExecuteReader();
        if (dr3.HasRows)
        {
            TextBox1.Text = "";
            note.Style.Add("display", "block");
            error.Style.Add("display", "none");
        }
        else
        {        
            dr3.Close();
            string query = "select * from student where (email = '" + TextBox1.Text.ToString() + "' or enrno = '"+ TextBox1.Text.ToString() +"' or mo = '"+ TextBox1.Text.ToString() +"') and pass = '" + TextBox2.Text + "' and status = 'yes'";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.HasRows)
            {
                dr.Read();
                Application["stid"] = dr["id"].ToString();
                Application["student"] = dr["name"].ToString();
                Application["stlname"] = dr["lname"].ToString();
                Application["stmo"] = dr["mo"].ToString();
                Application["stemail"] = dr["email"].ToString();
                Application["stream"] = dr["stream"].ToString();
                Response.Redirect("student");
            }
            else
            {
                dr.Close();
                string query1 = "select * from admin where email = '" + TextBox1.Text + "' and pass = '" + TextBox2.Text + "'";
                SqlCommand cmd1 = new SqlCommand(query1, con);
                SqlDataReader dr1 = cmd1.ExecuteReader();
                if (dr1.HasRows)
                {
                    dr1.Read();
                    Application["adminid"] = dr1["id"].ToString();
                    Application["admin"] = dr1["name"].ToString();
                    Application["adminmo"] = dr1["mo"].ToString();
                    Application["adminemail"] = dr1["email"].ToString();
                    Response.Redirect("librarian");
                }
                else
                {
                    TextBox1.Text = "";
                    error.Style.Add("display", "block");
                    note.Style.Add("display", "none");
                }
                dr1.Close();
            }
        }
    }
    public void sendmail()
    {
        try
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("library.1system@gmail.com", "Abcd@123");
            MailMessage msg = new MailMessage();
            msg.To.Add(TextBox1.Text.ToString());
            msg.From = new MailAddress("library.1system@gmail.com");
            msg.Subject = "From Library.com";
            msg.Body = " Your Activation Code is " + votp.ToString() + "\n\n\n ";
            client.Send(msg);
            Response.Write("<script> alert('Kindly verify your Account with actiovation code!'); </script>");
            //Response.Write("<script> window.location = 'login.aspx'; </script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script> alert('Could not send Email ! Please check Your connection!'); </script>");
        }
    }
}